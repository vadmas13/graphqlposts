import React, {useContext} from 'react'
import {Button, Card, Icon, Image, Label, Popup} from "semantic-ui-react";
import moment from 'moment'
import 'moment/locale/ru';
import {Link} from "react-router-dom";
import {AuthContext} from "../context/auth";
import LikeButton from "./LikeButton";
import DeleteButton from "./DeleteButton";
import MyPopup from "../util/MyPopup";


const PostCard = ({post}) => {
    const {username, id, createdAt, body, likeCount, likes, commentCount} = post;

    const {user} = useContext(AuthContext);

    const pressComments = () => {

    }

    return (
        <Card fluid>
            <Card.Content>
                <Image
                    floated='right'
                    size='mini'
                    src='https://react.semantic-ui.com/images/avatar/large/molly.png'
                />
                <Card.Header>{username}</Card.Header>
                <Card.Meta as={Link} to={`/posts/${id}`}>
                    {moment(createdAt).fromNow()}
                </Card.Meta>
                <Card.Description>
                    {body}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <LikeButton user={user} post={{id, likes, likeCount}}/>
                <MyPopup content={"Комментарии к посту"}
                         children={
                             <Button as='div' labelPosition='right' onClick={pressComments}>
                                 <Button color='teal'>
                                     <Icon name='comments'/>
                                 </Button>
                                 <Label basic color='teal' pointing='left'>
                                     {commentCount}
                                 </Label>
                             </Button>
                         }
                />

                {user && user.username === username &&
                <DeleteButton postId={id}/>
                }
            </Card.Content>
        </Card>
    )

}

export default PostCard