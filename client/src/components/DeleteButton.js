import React, {useState} from 'react'
import gql from 'graphql-tag'
import {Button, Confirm, Icon, Popup} from "semantic-ui-react";
import {useMutation} from "@apollo/react-hooks";
import {FETCH_POSTS_QUERY} from "../util/grqphql";
import MyPopup from "../util/MyPopup";

const DeleteButton = ({postId, callback, commentId}) => {

    const [confirmOpen, setConfirmOpen] = useState(false)

    const mutation = commentId ? DELETE_COMMENT_MUTATION : DELETE_POST_MUTATION;

    const [deletePostOrComment] = useMutation(mutation, {
        update(proxy) {
            setConfirmOpen(false);
            if (!commentId) {
                const data = proxy.readQuery({
                    query: FETCH_POSTS_QUERY
                })
                proxy.writeQuery({
                    query: FETCH_POSTS_QUERY,
                    data: {getPosts: [...data.getPosts.filter(post => post.id !== postId)]}
                })
            }
            if (callback) callback()
        },
        variables: {
            postId,
            commentId
        }
    })


    return (
        <>
            <MyPopup content={commentId? 'Удалить комментарий' : 'Удалить пост'}
                     children={
                         <Button as='div' onClick={() => setConfirmOpen(true)} color="red" floated="right">
                             <Icon name='trash' style={{margin: 0}}/>
                         </Button>
                     }
                     />
            <Confirm open={confirmOpen}
                     onConfirm={() => deletePostOrComment()}
                     onCancel={() => setConfirmOpen(false)}/>
        </>

    )
}

const DELETE_COMMENT_MUTATION = gql`
    mutation deleteComment($postId: ID!, $commentId: ID!){
        deleteComment(postId: $postId, commentId: $commentId){
            id
            comments{
                id
                username
                createdAt
                body
            }
            commentCount
        }
    }
`

const DELETE_POST_MUTATION = gql`
    mutation deletePost($postId: ID!){
        deletePost(postId: $postId)
    }
`

export default DeleteButton