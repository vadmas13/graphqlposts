import React, {useEffect, useState} from 'react'
import gql from 'graphql-tag'
import {Button, Icon, Label} from "semantic-ui-react";
import {Link} from "react-router-dom";
import {useMutation} from "@apollo/react-hooks";
import MyPopup from "../util/MyPopup";

const LikeButton = ({post: {id, likeCount, likes}, user}) => {

    const [liked, setLiked] = useState();

    useEffect(() => {
        if (user && likes.find(like => like.username === user.username)) {
            setLiked(true)
        } else setLiked(false)
    }, [user, likes])

    const [likePost, {loading}] = useMutation(LIKE_POST_MUTATION, {
        variables: {postId: id}
    })

    const likeButton = user ? (
        liked ?
                loading ? (
                    <Button color='red' loading>
                        <Icon name='heart'/>
                    </Button>
                ) : (
                    <Button color='red' onClick={likePost}>
                        <Icon name='heart'/>
                    </Button>
                )
         : loading ? (
                <Button color='red' loading basic>
                    <Icon name='heart'/>
                </Button>
            ):(
            <Button color='red' onClick={likePost} basic>
                <Icon name='heart'/>
            </Button>
        )
    ) : (
        <Button as={Link} to={'/login'} color='red' basic>
            <Icon name='heart'/>
        </Button>
    )


    return (
        <MyPopup content="Мне нравится"
                 children={
                     <Button as='div' labelPosition='right'>
                         {likeButton}
                         <Label basic color='red' pointing='left'>
                             {likeCount}
                         </Label>
                     </Button>
                 }
        />
    )
}

const LIKE_POST_MUTATION = gql`
    mutation likePost($postId: ID!){
        likePost(postId: $postId){
            id
            likes{
                id username
            }
            likeCount
        }
    }
`


export default LikeButton