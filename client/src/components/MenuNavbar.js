import React, {useContext, useState} from 'react'
import {Menu} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import {AuthContext} from "../context/auth";

const colorsA = [
    {
        name: 'Домой',
        url: 'home'
    },
    {
        name: 'Войти',
        url: 'login'
    },
    {
        name: 'Регистрация',
        url: 'register'
    }
]

const MenuNavbar = () => {

    const { user, logout } = useContext(AuthContext)

    const pathname = window.location.pathname;

    const path = pathname === '/' ? 'home' : pathname.substr(1);

    const [activeA, setActiveA] = useState(path)

    let handleAClick = (e, {name}) => {
        let itemUrl = colorsA.find(f => f.name === name)
        setActiveA(itemUrl.url)
    }

    const menuBar = user ? (
        <Menu inverted>
                <Menu.Item
                    key={'home'}
                    name={user.username}
                    active
                    color={"red"}
                    as={Link}
                    to={'/'}
                />

            <Menu.Menu position='right'>
                <Menu.Item
                    key={'login'}
                    name={'Выйти'}
                    onClick={logout}
                    color={"red"}
                />
            </Menu.Menu>
        </Menu>
    ) : (
        <Menu inverted>
            <Menu.Item
                key={'home'}
                name={'Домой'}
                active={activeA === 'home'}
                color={"red"}
                onClick={handleAClick}
                as={Link}
                to={'/'}
            />

            <Menu.Menu position='right'>
                <Menu.Item
                    key={'login'}
                    name={'Войти'}
                    active={activeA === 'login'}
                    color={"red"}
                    onClick={handleAClick}
                    as={Link}
                    to={'/login'}
                />
                <Menu.Item
                    key={'register'}
                    name={'Регистрация'}
                    active={activeA === 'register'}
                    color={"red"}
                    onClick={handleAClick}
                    as={Link}
                    to={'/register'}
                />
            </Menu.Menu>
        </Menu>
    )

    return menuBar;

}

export default MenuNavbar