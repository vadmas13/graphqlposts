import React, {useState} from 'react'
import {Button, Form, Transition} from "semantic-ui-react";
import gql from "graphql-tag";
import {useMutation} from "@apollo/react-hooks";
import {useForm} from "../util/hooks";
import {FETCH_POSTS_QUERY} from "../util/grqphql";

const PostForm = () => {

    const createPostCallback = () => createPost()

    const {onSubmit, onChange, values} = useForm(createPostCallback, {
        body: ''
    })

    const [errorBody, setErrors] = useState('');


    const [createPost] = useMutation(CREATE_POST_MUTATION, {
        variables: values,
        update(proxy, result) {
            const data = proxy.readQuery({
                query: FETCH_POSTS_QUERY
            })
            proxy.writeQuery({
                query: FETCH_POSTS_QUERY,
                variables: values,
                data: {getPosts: [result.data.createPost, ...data.getPosts]}
            });
            values.body = '';
            setErrors('');
        },
        onError(err) {
            setErrors(err.graphQLErrors[0].message);
        }
    })


    return (
        <Form onSubmit={onSubmit}>
            <h2>Создать пост</h2>
            <Form.Field>
                <Form.Input
                    placeholder='Hello World!'
                    name='body'
                    onChange={onChange}
                    error={!!errorBody}
                    value={values.body}
                />
                <Button type='submit' color='red'>
                    Создать пост
                </Button>
            </Form.Field>
            <Transition.Group>
                {errorBody && (
                    <div className="ui error message" style={{display: 'block'}}>
                        <ul className='list'>
                            <li key={errorBody}>{errorBody}</li>
                        </ul>
                    </div>
                )}
            </Transition.Group>
        </Form>
    )
}


const CREATE_POST_MUTATION = gql`
    mutation createPost($body: String!){
        createPost(body: $body){
            id body createdAt username
            likes{
                id username createdAt
            }
            likeCount
            comments{
                id body username createdAt
            }
            commentCount
        }
    }
`

export default PostForm