import React, {useContext} from 'react';
import {useQuery} from "@apollo/react-hooks";
import {Grid, Loader, Transition} from 'semantic-ui-react'
import PostCard from "../components/PostCard";
import {AuthContext} from "../context/auth";
import PostForm from "../components/PostForm";
import {FETCH_POSTS_QUERY} from "../util/grqphql";

const Home = () => {
    const {user} = useContext(AuthContext);

    const {loading, data} = useQuery(FETCH_POSTS_QUERY);

    return (
        <Grid columns='three' divided>
            {
                user && (
                    <Grid.Row>
                        <Grid.Column key='newPost'>
                            <PostForm/>
                        </Grid.Column>
                    </Grid.Row>
                )
            }
            <Grid.Row className='titleHeader'>
                <h1>
                    Недавние посты
                </h1>
            </Grid.Row>
            <Grid.Row>
                {loading ? (
                    <Loader/>
                ) : (
                    <Transition.Group>
                        {
                            data.getPosts && data.getPosts.map(post => (
                                <Grid.Column key={post.id} style={{marginBottom: 20}}>
                                    <PostCard post={post}/>
                                </Grid.Column>
                            ))
                        }
                    </Transition.Group>
                )}

            </Grid.Row>
        </Grid>
    );
}

export default Home;
