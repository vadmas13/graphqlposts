import React, {useContext, useRef, useState} from 'react'
import {Button, Card, Form, Grid, Icon, Image, Label, Loader} from "semantic-ui-react";
import moment from 'moment'
import 'moment/locale/ru';
import {AuthContext} from "../context/auth";
import gql from 'graphql-tag'
import {useMutation, useQuery} from "@apollo/react-hooks";
import LikeButton from "../components/LikeButton";
import DeleteButton from "../components/DeleteButton";


const SinglePageCard = (props) => {

    const postId = props.match.params.postId;
    let postMarkup;

    const {user} = useContext(AuthContext);

    const inputCommentRef = useRef(null);

    const [comment, setComment] = useState('');

    const [submitComment, { loading }] = useMutation(SUBMIT_COMMENT_MUTATION, {
        variables: {
            postId,
            body: comment
        },
        update() {
            setComment('');
            inputCommentRef.current.blur();
        }
    })

    const {data} = useQuery(FETCH_POST_QUERY, {
        variables: {
            postId
        }
    })

    const deletePostCallback = () => {
        props.history.push('/');
    }

    if (data && data.getPost) {
        const {id, body, createdAt, username, comments, likes, likeCount, commentCount} = data.getPost;

        postMarkup = (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Image
                            floated='right'
                            size='small'
                            src='https://react.semantic-ui.com/images/avatar/large/molly.png'
                        />
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Card fluid>
                            <Card.Content>
                                <Card.Header> {username} </Card.Header>
                                <Card.Meta>
                                    {moment(createdAt).fromNow()}
                                </Card.Meta>
                                <Card.Description>{body}</Card.Description>
                            </Card.Content>
                            <Card.Content extra>
                                <LikeButton user={user} post={{id, likeCount, likes}}/>
                                <Button as='div' labelPosition='right' onClick={() => console.log('comment')}>
                                    <Button color='teal'>
                                        <Icon name='comments'/>
                                    </Button>
                                    <Label basic color='teal' pointing='left'>
                                        {commentCount}
                                    </Label>
                                </Button>
                                {user && user.username === username && (
                                    <DeleteButton postId={id} callback={deletePostCallback}/>
                                )}
                            </Card.Content>
                        </Card>
                        {user && (
                            <Card fluid>
                                <Card.Content>
                                    <p>Добавить комментарий</p>
                                    <Form>
                                        <div className="ui action input fluid">
                                            <input type="text"
                                                   placeholder={'Комментарий...'}
                                                   value={comment}
                                                   ref={inputCommentRef}
                                                   onChange={(e) => setComment(e.target.value)}
                                            />
                                            { loading ? (
                                                <Button type="submit"
                                                        className='ui button teal'
                                                        loading
                                                        disabled>
                                                    Добавить
                                                </Button>
                                            ): (
                                                <Button type="submit"
                                                        className='ui button teal'
                                                        onClick={submitComment}
                                                        disabled={comment.trim() === ''}>
                                                    Добавить
                                                </Button>
                                            ) }

                                        </div>
                                    </Form>
                                </Card.Content>
                            </Card>
                        )}
                        {comments.map(comment => (
                            <Card fluid key={comment.id}>
                                <Card.Content>
                                    {user && user.username === comment.username && (
                                        <DeleteButton postId={id} commentId={comment.id}/>
                                    )}
                                    <Card.Header>
                                        {comment.username}
                                    </Card.Header>
                                    <Card.Meta>
                                        {moment(comment.createdAt).fromNow()}
                                    </Card.Meta>
                                    <Card.Description>
                                        {comment.body}
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        ))}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    } else {
        postMarkup = <Loader/>;
    }

    return postMarkup

}

const SUBMIT_COMMENT_MUTATION = gql`
    mutation($postId: ID!, $body: String!){
        createComment(postId: $postId, body: $body){
            id
            comments{
                id
                username
                createdAt
                body
            }
            commentCount
        }
    }
`

const FETCH_POST_QUERY = gql`
    query getPost($postId: ID!){
        getPost(postId: $postId){
            id
            username
            createdAt
            likeCount
            body
            commentCount
            likes{
                username
            }
            comments{
                id
                body
                username
                createdAt
            }
        }
    }
`

export default SinglePageCard