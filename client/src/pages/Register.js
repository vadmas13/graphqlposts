import React, {useContext, useState} from 'react';
import { Button, Form } from 'semantic-ui-react'
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import {useForm} from "../util/hooks";
import {AuthContext} from "../context/auth";

const Register = (props) => {

    const context = useContext(AuthContext);

    const registerUser = () => addUser()

    const { onChange, onSubmit, values } = useForm(registerUser, {
        username: '',
        password: '',
        confirmPassword: '',
        email: ''
    })

    const [errors, setErrors] = useState({})

    const [addUser, { loading }] = useMutation(REGISTER_USER, {
        update(__, {data: {register: userData}}){
            context.login(userData);
            props.history.push('/')
        },
        onError(err){
            setErrors(err.graphQLErrors[0].extensions.errors)
        },
        variables: {
            ...values
        }
    })


    return (
        <div className='form-container'>
            <Form onSubmit={onSubmit} className={loading ? 'loading' : ''}>
            <h1>Регистрация</h1>
                <Form.Input
                    key="username"
                    label="Логин"
                    type="text"
                    placeholder="Логин..."
                    name="username"
                    error={!!errors.username}
                    value={values.username}
                    onChange={onChange} />
                <Form.Input
                    key="email"
                    label="Email"
                    type="text"
                    placeholder="lol@ya.com"
                    name="email"
                    error={!!errors.email}
                    value={values.email}
                    onChange={onChange} />
                <Form.Input
                    key="password"
                    label="Пароль"
                    placeholder="Пароль"
                    type="password"
                    name="password"
                    error={!!errors.password}
                    value={values.password}
                    onChange={onChange} />
                <Form.Input
                    key="confirmPassword"
                    label="Подтверждение"
                    placeholder="Подтвердите пароль"
                    type="password"
                    error={!!errors.confirmPassword}
                    name="confirmPassword"
                    value={values.confirmPassword}
                    onChange={onChange} />
                <Button primary>Регистрация</Button>
            </Form>
            { Object.keys(errors).length > 0 && (
                <div className="ui error message">
                    <ul className='list'>
                        { Object.values(errors).map(value => (
                            <li key={value}>{ value }</li>
                        )) }
                    </ul>
                </div>
            ) }
        </div>
    );
}

const REGISTER_USER = gql`
    mutation register(
        $username: String!
        $email: String!
        $password: String!
        $confirmPassword: String!
    ){
        register(
            registerInput: {
                username: $username
                email: $email
                password: $password
                confirmPassword: $confirmPassword
            }
        ){
            id email username token createdAt
        }
    }
`

export default Register;
