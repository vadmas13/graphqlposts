import React, {useContext, useState} from 'react';
import { Button, Form } from 'semantic-ui-react'
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import {useForm} from "../util/hooks";
import {AuthContext} from "../context/auth";

const Login = (props) => {

    const context = useContext(AuthContext);

    const loginUser = () => login()

    const { onChange, onSubmit, values } = useForm(loginUser, {
        username: '',
        password: ''
    })

    const [errors, setErrors] = useState({})


    const [login, { loading }] = useMutation(REGISTER_USER, {
        update(__, { data: { login: userData}}){
            context.login(userData)
            props.history.push('/')
        },
        onError(err){
            setErrors(err.graphQLErrors[0].extensions.errors)
        },
        variables: {
            ...values
        }
    })


    return (
        <div className='form-container'>
            <Form onSubmit={onSubmit} className={loading ? 'loading' : ''}>
                <h1>Регистрация</h1>
                <Form.Input
                    key="username"
                    label="Логин"
                    type="text"
                    placeholder="Логин..."
                    name="username"
                    error={!!errors.username}
                    value={values.username}
                    onChange={onChange} />
                <Form.Input
                    key="password"
                    label="Пароль"
                    placeholder="Пароль"
                    type="password"
                    name="password"
                    error={!!errors.password}
                    value={values.password}
                    onChange={onChange} />
                <Button primary>Войти</Button>
            </Form>
            { Object.keys(errors).length > 0 && (
                <div className="ui error message">
                    <ul className='list'>
                        { Object.values(errors).map(value => (
                            <li key={value}>{ value }</li>
                        )) }
                    </ul>
                </div>
            ) }
        </div>
    );
}

const REGISTER_USER = gql`
    mutation login(
        $username: String!
        $password: String!
    ){
        login(
            username: $username
            password: $password
        ){
            id email username token createdAt
        }
    }
`

export default Login;
