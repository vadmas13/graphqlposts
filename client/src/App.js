import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import 'semantic-ui-css/semantic.min.css'
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import MenuNavbar from "./components/MenuNavbar";
import {Container} from "semantic-ui-react";
import {AuthProvider} from "./context/auth";
import AuthRoute from "./util/AuthRoute";
import SinglePageCard from "./pages/SinglePageCard";


const App = () => {
    return (
        <AuthProvider>
            <Router>
                <Container>
                    <MenuNavbar/>
                    <Route exact path={'/'} component={Home}></Route>
                    <AuthRoute exact path={'/login'} component={Login}></AuthRoute>
                    <AuthRoute exact path={'/register'} component={Register}></AuthRoute>
                    <Route exact path="/posts/:postId" component={SinglePageCard} />
                </Container>
            </Router>
        </AuthProvider>
    );
}

export default App;
